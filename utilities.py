from datetime import datetime


# Генератор словаря задач
def dict_generator(num: int) -> dict[str, dict[str, str | datetime]]:
    return {
        f"task_{i + 1}": {"start_time": datetime.now(), "status": "pending"}
        for i in range(num)
    }


def dict_with_complete_task_generator(num: int) -> dict[str, dict[str, str | datetime]]:
    return {
        f"task_{i + 1}": {"start_time": datetime.now(), "status": "complete"}
        for i in range(num)
    }
