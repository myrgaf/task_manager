from datetime import datetime


class TaskManager:
    def __init__(self, initial_tasks: dict = None, capacity: int = 10):
        self.tasks = initial_tasks or {}
        self.capacity = capacity

    def add_task(self, task_name: str, start_time: None | datetime = None):
        if len(self.tasks) >= self.capacity:
            raise ValueError('Task Manager is at full capacity. Cannot add more tasks.')
        if task_name in self.tasks:
            raise ValueError('Task already exist in Task Manager.')
        self.tasks[task_name] = {
            "start_time": start_time or datetime.now(),
            "status": "pending"
        }

    def remove_task(self, task_name: str) -> bool:
        if task_name in self.tasks:
            del self.tasks[task_name]
            return True
        return False

    def remove_all_tasks(self):
        self.tasks.clear()

    def remove_completed_tasks(self):
        self.tasks = {task: info for task, info in self.tasks.items() if
                      info["status"] != "completed"}

    def get_active_tasks(self) -> dict:
        return {task: info for task, info in self.tasks.items() if
                info["status"] != "completed" and info['start_time'] <= datetime.now()}

    def complete_task(self, task_name) -> bool:
        if task_name in self.tasks:
            self.tasks[task_name]["status"] = "completed"
            return True
        return False
