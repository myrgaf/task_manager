from task_manager import TaskManager
from utilities import dict_generator, dict_with_complete_task_generator
from datetime import datetime
import pytest


# Пустой экземпляр
@pytest.fixture
def empty_task_manager():
    return TaskManager()


# Полный экземпляр
@pytest.fixture(params=[10])
def full_task_manager(request):
    example_task_manager = TaskManager()
    full_dict = dict_generator(request.param)
    for key in full_dict.keys():
        example_task_manager.add_task(task_name=key, start_time=datetime.now())
    return example_task_manager


# @pytest.fixture(params=[0, 5, 9])
# def complete_task_manager(request):
#     example_task_manager = TaskManager()
#     full_dict = dict_with_complete_task_generator(request.param)
#     for key in full_dict.keys():
#         example_task_manager.add_task(task_name=key, start_time=datetime.now())
#     return example_task_manager
