from conftest import dict_generator
from datetime import datetime
import pytest


# Проверки функции добавления задач
def test_add_one_task(empty_task_manager):
    empty_task_manager.add_task(task_name='task_1', start_time=datetime.now())
    right_dict = dict_generator(1)
    assert empty_task_manager.tasks == right_dict


def test_same_task(empty_task_manager):
    empty_task_manager.add_task(task_name='task_1', start_time=datetime.now())
    with pytest.raises(ValueError):
        empty_task_manager.add_task(task_name='task_1', start_time=datetime.now())


def test_full_capacity_task(full_task_manager):
    with pytest.raises(Exception):
        full_task_manager.add_task(task_name='task_11', start_time=datetime.now())


# Проверка функции удаление задач
def test_remove_task(empty_task_manager):
    empty_task_manager.add_task(task_name='task_1', start_time=datetime.now())
    assert empty_task_manager.remove_task(task_name='task_1') is True


def test_remove_non_existent_task(empty_task_manager):
    assert empty_task_manager.remove_task(task_name='non_existent_task') is False


# Проверка функции удаление всех задач
def test_remove_all_tasks(full_task_manager):
    full_task_manager.remove_all_tasks()
    assert full_task_manager.tasks == {}


# Проверка функции удаление завершенных задач
# def test_remove_complete_tasks(complete_task_manager):
#     complete_task_manager.remove_completed_tasks()
#     assert len(complete_task_manager.tasks) == 0


# Проверка функции получения незавершенных задач
def test_get_active_tasks(full_task_manager):
    assert len(full_task_manager.get_active_tasks()) == 10


# Проверка функции завершения задачи
def test_complete_task(empty_task_manager):
    empty_task_manager.add_task(task_name='task_1', start_time=datetime.now())
    assert empty_task_manager.complete_task(task_name='task_1') is True


def test_complete_existed_task(empty_task_manager):
    assert empty_task_manager.complete_task(task_name='existed task') is False


